package com.example.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.CommentDto;
import com.example.dto.StoryDto;
import com.example.service.ServiceImpl;
import com.example.util.StoriesObserver;

@RestController
@RequestMapping("/v0")
public class Controller {

	@Autowired
	private ServiceImpl newsService;
	
	
	@GetMapping("/top-stories")
	public List<StoryDto> getTopStories(){
		List topStories=StoriesObserver.getTopStories();
		Collections.reverse(topStories);
		return topStories;
	}
	
	@GetMapping("/past-stories")
	public List<StoryDto> getPastStories(){
		List pastStories=StoriesObserver.getPastStories();
		Collections.reverse(pastStories);
		return pastStories;
	}
	
	@Cacheable(value = "comments", key = "#id" )
	@GetMapping("/comments/{id}")
	public List<CommentDto> getComments(@PathVariable Long id){
		return newsService.getTopComments(id);
	}
}
