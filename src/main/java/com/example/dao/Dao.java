package com.example.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.example.dto.ItemDto;
import com.example.dto.UserDto;

@Repository
public class Dao {

	@Autowired
	private RestTemplate restTemplate;

	public String getMaxId() {
		// TODO Auto-generated method stub
		return restTemplate.getForObject("https://hacker-news.firebaseio.com/v0/maxitem.json?print=pretty", String.class);
	}
	
	public ResponseEntity<ItemDto> getItemDetails(Long id) {
		return restTemplate.getForEntity("https://hacker-news.firebaseio.com/v0/item/"+id+".json?print=pretty", ItemDto.class);
	}
	
//	@Cacheable(value = "users", key = "#id")
	public ResponseEntity<UserDto> getUserDetails(String id) {
		return restTemplate.getForEntity("https://hacker-news.firebaseio.com/v0/user/"+id+".json?print=pretty", UserDto.class);
	}
}
