package com.example.util;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;

import com.example.dto.StoryDto;

public class StoriesObserver {

	private static BlockingQueue<StoryDto> topStories=new ArrayBlockingQueue(10);
	private static BlockingQueue<StoryDto> pastStories=new ArrayBlockingQueue(10);

	private StoriesObserver() {
	}

	public static void updateStories(Queue<StoryDto> newStories) throws InterruptedException {
		pastStories.clear();
		for(StoryDto story:topStories)
			pastStories.put(story);

		topStories.clear();
		for(StoryDto story:newStories)
			topStories.put(story);
	}

	public static List<StoryDto> getTopStories(){
		return topStories.stream().collect(Collectors.toList());
	}

	public static List<StoryDto> getPastStories(){
		return pastStories.stream().collect(Collectors.toList());
	}
}
