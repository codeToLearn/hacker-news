package com.example.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ItemDto implements Serializable{

	private String by;
	private Long id;
	private Integer descendants;
	private List<Long> kids;
	private Integer score;
	private String parent;
	private String text;
	private Date time;
	private String type;
	private String title;
	private String url;
	private boolean deleted;
	
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getBy() {
		return by;
	}
	public void setBy(String by) {
		this.by = by;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<Long> getKids() {
		return kids;
	}
	public void setKids(List<Long> kids) {
		this.kids = kids;
	}
	public Integer getDescendants() {
		return descendants;
	}
	public void setDescendants(Integer descendants) {
		this.descendants = descendants;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
