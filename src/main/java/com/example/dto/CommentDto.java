package com.example.dto;

import java.io.Serializable;

public class CommentDto implements Serializable{

	private String text;
	private String by;
	private Integer age;
	private Integer subCommentcount;


	public CommentDto(String text, String by, Integer age, Integer count) {
		super();
		this.text = text;
		this.by = by;
		this.age = age;
		this.subCommentcount = count;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getBy() {
		return by;
	}
	public void setBy(String by) {
		this.by = by;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Integer getSubCommentcount() {
		return subCommentcount;
	}
	public void setSubCommentcount(Integer subCommentcount) {
		this.subCommentcount = subCommentcount;
	}


}
