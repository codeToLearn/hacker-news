package com.example.dto;

import java.io.Serializable;
import java.util.Date;

public class StoryDto implements Serializable{

	private String title;
	private String url;
	private Integer score;
	private Date time;
	private String by;
	
	
	public StoryDto(String title, String url, Integer score, Date time, String by) {
		super();
		this.title = title;
		this.url = url;
		this.score = score;
		this.time = time;
		this.by = by;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getBy() {
		return by;
	}
	public void setBy(String by) {
		this.by = by;
	}

}
