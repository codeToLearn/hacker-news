package com.example.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.example.dao.Dao;
import com.example.dto.CommentDto;
import com.example.dto.ItemDto;
import com.example.dto.StoryDto;
import com.example.dto.UserDto;
import com.example.util.StoriesObserver;

@Service
public class ServiceImpl{

	@Autowired
	private Dao newsDao;

	@Scheduled(fixedDelay = 600000, initialDelay = 3000)
	public void updateTopStories() throws InterruptedException{ 
		//get max id
		Long maxId=Long.parseLong(newsDao.getMaxId().trim());
		
		//get time before 10 min
		Date date=new Date();
		long t= date.getTime();
		date=new Date(t - 600000);
		ItemDto itemDto;
		
		//Initialise a priority queue to record top 10 stories
		Queue<StoryDto> queue=new PriorityQueue<StoryDto>(10, new Comparator<StoryDto>() {

				@Override
				public int compare(StoryDto o1, StoryDto o2) {
					// TODO Auto-generated method stub
					if(o1.getScore()==o2.getScore())
					return 0;
					if(o1.getScore()>o2.getScore())
						return 1;
					return -1;
				}
			});
		
		int count=0;
		do {
			ResponseEntity<ItemDto> entity =newsDao.getItemDetails(maxId--);
			itemDto=entity.getBody();
			StoryDto tempItem=queue.peek();
			System.out.println(itemDto.getId()+"  "+count++);
			//validate a story and its top score
			if("story".equals(itemDto.getType()) && !itemDto.isDeleted() && (queue.size()<10 || tempItem.getScore()<itemDto.getScore())) {
				StoryDto story=new StoryDto(itemDto.getTitle(),itemDto.getUrl(),itemDto.getScore(),itemDto.getTime(),itemDto.getBy());
				if(queue.size()==10)
					queue.poll();
				queue.add(story);
			}
			
		}while(itemDto.getTime().after(date) || queue.size()<10);
		StoriesObserver.updateStories(queue);
	}
	
	
	public List<CommentDto> getTopComments(Long id) {
		ItemDto story=newsDao.getItemDetails(id).getBody();
		if(story.getKids()==null || story.getKids().size()==0)
			return new ArrayList<CommentDto>();
		
		CommentDto commentDto;
		List<CommentDto> topComments=new ArrayList<CommentDto>();
		ItemDto item;
		for(Long ids:story.getKids()) {
			item=newsDao.getItemDetails(ids).getBody();
			commentDto=new CommentDto(item.getText(),item.getBy(),null,countSubComments(item));
			topComments.add(commentDto);
		}
		
		//sort on the basis of sub comment count
		Collections.sort(topComments,new Comparator<CommentDto>() {

			@Override
			public int compare(CommentDto o1, CommentDto o2) {
				// TODO Auto-generated method stub
				return o1.getSubCommentcount().compareTo(o2.getSubCommentcount());
			}
		});
		//extract top 10 comments
		topComments=topComments.stream().limit(10).collect(Collectors.toList());
		int currYear=new Date().getYear();
		
		//adding required user details
		for(CommentDto comment:topComments) {
			UserDto user=newsDao.getUserDetails(comment.getBy()).getBody();
			comment.setAge(currYear-user.getCreated().getYear());
		}
		return topComments;
	}
	
	//method to recursively count the sub comments
	private int countSubComments(ItemDto comment) {
		if(comment.getKids()==null ||comment.getKids().size()==0) {
			return 1;
		}
		int count =1;
		for(Long ids:comment.getKids()) {
			ItemDto item=newsDao.getItemDetails(ids).getBody();
			count+=countSubComments(item);
		}
		return count;
	}
}
